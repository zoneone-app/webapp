declare module "@moxy/next-layout" {
    import {ComponentType, PureComponent, ReactElement, ReactNode} from "react";

    export interface LayoutTreeProps extends AppProps {
        Component: ComponentType;
        pageProps: unknown;
        pageKey?: string;
        defaultLayout?: ReactElement;
        children?: ReactNode;
    }

    export class LayoutTree extends PureComponent<LayoutTreeProps> {}

    type MapFunction<T> = (state: T) => ReactElement;
    type ILSFunction<T, Props> = (props: Props) => T;

    type WithLayoutResult = <Props>(
        component: ComponentType<Props>
    ) => ComponentType<Props>;

    export function withLayout<T, Props = never>(
        mapLayoutStateToLayoutTree: MapFunction<T>,
        initialLayoutState: T | ILSFunction<T, Props>
    ): WithLayoutResult<Props>;

    export function withLayout(
        mapLayoutStateToLayoutTree?: ReactElement
    ): WithLayoutResult<unknown>;
}
