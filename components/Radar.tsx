import React, {FC, Suspense, useCallback, useContext, useEffect} from "react";
import {GeoJSONLayer, MapContext} from "react-mapbox-gl";
import {VectorSource} from "mapbox-gl";
import styled from "@emotion/styled";
import {useRouter} from "next/router";
import {usePageData, usePageLoader} from "../utils/pageData";
import {useApi} from "../utils/api/useApi";
import {Map as MapDisplay} from "./Map";

interface StopsLayerProps {
    highlightStop?: string;
}

const StopsLayer: FC<StopsLayerProps> = props => {
    const map = useContext(MapContext);
    const {set: setPageData} = usePageLoader();
    const {push} = useRouter();
    const api = useApi();

    const handleMouseEnter = useCallback(() => {
        map.getCanvas().style.cursor = "pointer";
    }, [map]);

    const handleMouseLeave = useCallback(() => {
        map.getCanvas().style.cursor = "";
    }, [map]);

    const handleClick = useCallback(
        (e: mapboxgl.MapLayerMouseEvent) => {
            const feature = e.features[0];
            const id = feature.properties.id;

            setPageData(() => ({stop: {id, value: api.stops().getById(id)}}));
            push(
                {pathname: "/stops/[id]", query: {id}, hash: location.hash},
                null,
                {shallow: true}
            );
        },
        [map]
    );

    const stopTileUrl = api.getStopTileUrl().suspense();

    useEffect(() => {
        const sourceOpts: VectorSource = {
            type: "vector",
            tiles: [stopTileUrl],
            attribution: "ZoneONE"
        };

        map.addSource("stops_source", sourceOpts);

        map.addLayer({
            id: "stops",
            type: "circle",

            source: "stops_source",
            "source-layer": "stops",

            paint: {
                "circle-color": "white",
                "circle-stroke-color": "black",
                "circle-stroke-width": 1,
                "circle-radius": 3,
                "circle-opacity": 1
            }
        });

        map.on("mouseenter", "stops", handleMouseEnter)
            .on("mouseleave", "stops", handleMouseLeave)
            .on("click", "stops", handleClick);

        return () => {
            if (!map.isStyleLoaded()) {
                if (process.env.NODE_ENV !== "production")
                    console.warn(
                        "Skipping removing event listeners as map is unloaded"
                    );
                return;
            }

            map.off("mouseenter", "stops", handleMouseEnter)
                .off("mouseleave", "stops", handleMouseLeave)
                .off("click", "stops", handleClick);

            map.removeLayer("stops").removeSource("stops_source");
        };
    }, [handleMouseEnter, handleMouseLeave, map]);

    useEffect(() => {
        if (!props.highlightStop) {
            map.setPaintProperty("stops", "circle-radius", 3);
            map.setPaintProperty("stops", "circle-opacity", 1);
        } else {
            map.setPaintProperty("stops", "circle-radius", [
                "case",
                ["==", ["get", "id"], props.highlightStop],
                6,
                3
            ]);
            map.setPaintProperty("stops", "circle-opacity", [
                "case",
                ["==", ["get", "id"], props.highlightStop],
                1,
                0.5
            ]);
        }
    }, [map, props.highlightStop]);

    return <></>;
};

const Map = styled(MapDisplay)`
    position: relative;
`;

export const Radar: FC = () => {
    const pageData = usePageData();
    const activeStopId = pageData.stop?.id;

    const {done: geoJsonLoaded, value: geoJson} = pageData.mapGeoJson || {};

    return (
        <Map>
            <Suspense fallback={null}>
                <StopsLayer highlightStop={activeStopId} />
            </Suspense>
            {geoJsonLoaded && (
                <GeoJSONLayer
                    data={geoJson}
                    lineLayout={{
                        "line-join": "round",
                        "line-cap": "round"
                    }}
                    linePaint={{
                        "line-color": "black",
                        "line-width": 2
                    }}
                />
            )}
        </Map>
    );
};
