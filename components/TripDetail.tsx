import React, {FC, MutableRefObject} from "react";
import {Box} from "@chakra-ui/react";

export const TripDetailContainer: FC<{
    containerRef?: MutableRefObject<HTMLDivElement>;
}> = props => (
    <Box
        paddingTop={2}
        paddingBottom={2}
        margin="0 1rem"
        ref={props.containerRef}
    >
        {props.children}
    </Box>
);
