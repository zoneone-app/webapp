import {UrlObject} from "url";
import React, {
    CSSProperties,
    FC,
    MutableRefObject,
    useEffect,
    useState
} from "react";
import {addMinutes} from "date-fns";
import StopTrip from "../utils/api/StopTrip";
import Stop from "../utils/api/Stop";
import {ListingLink} from "./ListingLink";
import {TripDetailContainer} from "./TripDetail";
import {RouteLabel} from "./sidebar/heading/RouteLabel";
import {TimeDisplay} from "./TimeDisplay";
import {SidebarLabelFallback} from "./sidebar/heading/SidebarLabel";
import {ThreeDotsSpinner} from "./spinners/ThreeDotsSpinner";

interface StopRouteDetailProps {
    trip: StopTrip;
    stop: Stop;
    nodeRef?: MutableRefObject<HTMLDivElement>;

    onLoadingChange(value: boolean): void;
}

const StopRouteDetail: FC<StopRouteDetailProps> = props => {
    const {
        value: tripStop,
        loading: tripStopLoading
    } = props.trip.trip.getStopById(props.stop.id());
    const {
        value: lastStop,
        loading: lastStopLoading
    } = props.trip.trip.getStopByIdx(-1);
    const {value: code, loading: codeLoading} = props.trip.route.code;
    const {value: styling, loading: stylingLoading} = props.trip.route.styling;

    const approx = tripStop && !(tripStop.isTimingPoint || tripStop.dwell > 0);

    const loading =
        tripStopLoading || lastStopLoading || codeLoading || stylingLoading;
    useEffect(() => props.onLoadingChange(loading), [loading]);

    if (loading) return <SidebarLabelFallback size="md" />;

    return (
        <TripDetailContainer containerRef={props.nodeRef}>
            <RouteLabel
                lastStop={lastStop}
                code={code}
                styling={styling}
                details={
                    <TimeDisplay
                        time={props.trip.arrivalTime}
                        endTime={
                            tripStop.dwell &&
                            addMinutes(props.trip.arrivalTime, tripStop.dwell)
                        }
                        approximate={approx}
                        preText="Sch "
                    />
                }
                size="md"
            />
        </TripDetailContainer>
    );
};

export interface StopTripDetailProps {
    stop: Stop;
    trip: StopTrip;
    isPreviousLoading?: boolean;
    scrollNode?: HTMLDivElement;
    style?: CSSProperties;
}

export const StopTripDetail: FC<StopTripDetailProps> = props => {
    const [isLoading, setIsLoading] = useState(false);

    const linkHref: UrlObject = {
        pathname: "/trips/[id]",
        query: {
            id: props.trip.tripId
        }
    };

    const displayLoader = !props.isPreviousLoading;

    return (
        <ThreeDotsSpinner
            size="md"
            isLoading={isLoading}
            isHidden={!displayLoader}
            style={props.style}
        >
            <ListingLink href={linkHref}>
                <StopRouteDetail
                    trip={props.trip}
                    stop={props.stop}
                    onLoadingChange={setIsLoading}
                />
            </ListingLink>
        </ThreeDotsSpinner>
    );
};
