import React, {FC} from "react";
import {Flex, Text, ThemingProps, useStyleConfig} from "@chakra-ui/react";
import {RouteStyling} from "../utils/api/Route";
import {SusRes} from "../utils/api/types";

const colourMappings: {[key in RouteStyling]: string} = {
    // Translink Bus
    [RouteStyling.BusExpress]: "matLightGreen.800",
    [RouteStyling.BusStandard]: "matLightGreen.600",
    [RouteStyling.GoldCoastExpress]: "matLightGreen.800",
    [RouteStyling.NightLink]: "gray.900",
    // TfB
    [RouteStyling.TfbStandard]: "blue.500",
    [RouteStyling.TfbExpress]: "blue.900",
    [RouteStyling.TfbBuz]: "red.700",
    [RouteStyling.CityGliderBlue]: "blue.200",
    [RouteStyling.CityGliderMaroon]: "pink.900",
    [RouteStyling.CityLoop]: "red.500",
    [RouteStyling.SpringHill]: "yellow.300",
    [RouteStyling.MtCoottha]: "blue.300",
    [RouteStyling.StationLink]: "teal.400",
    // Ferry
    [RouteStyling.Ferry]: "blue.600",
    [RouteStyling.CityHopper]: "red.500",
    // Tram
    [RouteStyling.Glink]: "yellow.400",
    // Train
    [RouteStyling.TrainDarkBlue]: "blue.900",
    [RouteStyling.TrainGreen]: "matGreen.700",
    [RouteStyling.TrainGrey]: "gray.500",
    [RouteStyling.Train]: "gray.500", // Dummy
    [RouteStyling.TrainLightBlue]: "blue.500",
    [RouteStyling.TrainPurple]: "purple.600",
    [RouteStyling.TrainRed]: "red.600",
    [RouteStyling.TrainYellow]: "yellow.600"
};

export interface RouteBulletSusProps {
    code: SusRes<string>;
    styling: SusRes<RouteStyling>;
}

export interface RouteBulletProps {
    code: string;
    styling: RouteStyling;
}

export const RouteBulletSus: FC<ThemingProps & RouteBulletSusProps> = props => {
    const code = props.code();
    const styling = props.styling();

    return <RouteBullet {...props} code={code} styling={styling} />;
};

export const RouteBullet: FC<ThemingProps & RouteBulletProps> = props => {
    const baseCode = props.code;
    const hasModifier = baseCode.startsWith("P");
    const withoutModifier = hasModifier ? baseCode.substring(1) : baseCode;
    const modifier = hasModifier && baseCode.substring(0, 1);

    const size = props.size || "md";
    const modifierStyle = `routes/prefix-${size}`;
    const numberStyle = `routes/number-${size}`;

    const {variant, colorScheme} = props;
    const styles = useStyleConfig("RouteBullet", {size, variant, colorScheme});

    return (
        <Flex
            align="center"
            justify="center"
            backgroundColor={colourMappings[props.styling]}
            color="white"
            sx={styles}
        >
            {hasModifier && <Text textStyle={modifierStyle}>{modifier}</Text>}
            <Text textStyle={numberStyle}>{withoutModifier}</Text>
        </Flex>
    );
};

export const style = {
    sizes: {
        md: {
            borderRadius: "0.5625rem",
            width: "3.375rem",
            height: "1.125rem"
        },
        lg: {
            borderRadius: "0.875rem",
            width: "5.25rem",
            height: "1.75rem"
        }
    }
};
