import {Box, chakra, SimpleGrid} from "@chakra-ui/react";
import styled from "@emotion/styled";
import React, {FC} from "react";
import noop from "../utils/noop";
import {Navigation} from "./Navigation";
import {RootSearch} from "./RootSearch";
import {Radar} from "./Radar";

const Nav = styled(Navigation)`
    grid-area: nav;
`;

const RadarWrapper = chakra(Radar);

export const Layout: FC = props => {
    return (
        <SimpleGrid
            templateAreas={[
                `"nav" "search" "map" "side"`,
                `"nav search" "map side"`
            ]}
            templateColumns={["1fr", "1fr 25rem"]}
            templateRows={["3.25rem 3.25rem 100vh auto", "3.25rem 1fr"]}
            width="100vw"
            height="100vh"
        >
            <Nav />

            <Box gridArea="search">
                <RootSearch onSubmit={noop} />
            </Box>

            <RadarWrapper gridArea="map" boxShadow="inner" />

            <Box gridArea="side">{props.children}</Box>
        </SimpleGrid>
    );
};
