import React, {FC} from "react";
import {SusRes} from "../../../utils/api/types";
import {getRouteNameDetails} from "../../../utils/getRouteNameDetails";
import {RouteBullet, RouteBulletSus} from "../../RouteBullet";
import {RouteStyling} from "../../../utils/api/Route";
import {TripStop} from "../../../utils/api/Trip";
import {SidebarLabel, SidebarLabelDetails, SizeProps} from "./SidebarLabel";

interface RouteLabelProps extends SizeProps {
    lastStop: TripStop;
    code: string;
    styling: RouteStyling;
    details?: SidebarLabelDetails;
}

interface RouteLabelSusProps extends SizeProps {
    lastStop: SusRes<TripStop>;
    code: SusRes<string>;
    styling: SusRes<RouteStyling>;
    details?: SidebarLabelDetails;
}

export const RouteLabel: FC<RouteLabelProps> = props => {
    const {lastStop} = props;
    const lines = getRouteNameDetails(
        null /*TODO: lastStop.stop.station.name()*/,
        lastStop.stop.suburb()
    );

    return (
        <SidebarLabel
            icon={
                <RouteBullet
                    code={props.code}
                    styling={props.styling}
                    size={props.size}
                />
            }
            details={props.details}
            size={props.size}
            {...lines}
        />
    );
};

export const RouteLabelSus: FC<RouteLabelSusProps> = props => {
    const lastStop = props.lastStop();
    const lines = getRouteNameDetails(
        null /*TODO: lastStop.stop.station.name()*/,
        lastStop.stop.suburb()
    );

    return (
        <SidebarLabel
            icon={
                <RouteBulletSus
                    code={props.code}
                    styling={props.styling}
                    size={props.size}
                />
            }
            details={props.details}
            size={props.size}
            {...lines}
        />
    );
};
