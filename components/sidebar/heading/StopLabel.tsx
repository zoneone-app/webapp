import React, {FC, ReactNode} from "react";
import {Image} from "@chakra-ui/react";
import {SusRes} from "../../../utils/api/types";
import {getStopNameDetails} from "../../../utils/getStopNameDetails";
import busStopIcon from "../../../assets/svg/stop-icons/bus.svg";
import {SidebarLabel, SidebarLabelDetails, SizeProps} from "./SidebarLabel";

interface StopLabelProps extends SizeProps {
    name: SusRes<string>;
    suburb?: SusRes<string>;
    details?: SidebarLabelDetails;

    sideContainer?: (child: ReactNode) => ReactNode;
}

export const StopLabel: FC<StopLabelProps> = props => {
    const lines = getStopNameDetails(props.name(), props.suburb?.());

    const iconSize = props.size === "lg" ? 7 : props.size === "md" ? 5 : 0;

    const innerIcon = (
        <Image src={busStopIcon} alt="Bus stop" width={iconSize} />
    );

    const icon = props.sideContainer
        ? props.sideContainer(innerIcon)
        : innerIcon;

    return (
        <SidebarLabel
            icon={icon}
            details={props.details}
            size={props.size}
            {...lines}
        />
    );
};
