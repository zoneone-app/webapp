import {Flex, Box, Text} from "@chakra-ui/react";
import React, {FC, ReactNode} from "react";
import {ThreeDotsSpinner} from "../../spinners/ThreeDotsSpinner";

export interface SizeProps {
    size: "lg" | "md";
}

export interface DetailedLine {
    detail: string;
    rest: string;
}

const LineOne: FC<SizeProps> = props => {
    const textStyle =
        props.size === "lg"
            ? "title-md-normal"
            : props.size === "md"
            ? "body"
            : null;

    return <Text textStyle={textStyle}>{props.children}</Text>;
};

const DetailedLineOne: FC<DetailedLine & SizeProps> = props => {
    const detailTextStyle =
        props.size === "lg"
            ? "title-md"
            : props.size === "md"
            ? "body-bold"
            : null;

    return (
        <LineOne size={props.size}>
            <Text as="span" textStyle={detailTextStyle}>
                {props.detail}
            </Text>
            , {props.rest}
        </LineOne>
    );
};

const LineTwo: FC<SizeProps> = props => {
    const textStyle =
        props.size === "lg"
            ? "title-sm"
            : props.size === "md"
            ? "subheadline"
            : null;

    return <Text textStyle={textStyle}>{props.children}</Text>;
};

export interface SidebarLabelLines {
    lineOne: string | DetailedLine;
    lineTwo?: string;
}

export type SidebarLabelIcon = ReactNode;
export type SidebarLabelDetails = ReactNode;

export interface SidebarLabelProps extends SidebarLabelLines, SizeProps {
    icon: SidebarLabelIcon;
    details?: SidebarLabelDetails;
}

export const SidebarLabel: FC<SidebarLabelProps> = props => {
    return (
        <Flex align="stretch" position="relative">
            {props.icon}
            <Box ml={4}>
                {typeof props.lineOne === "string" ? (
                    <LineOne size={props.size}>{props.lineOne}</LineOne>
                ) : (
                    <DetailedLineOne size={props.size} {...props.lineOne} />
                )}
                <LineTwo size={props.size}>{props.lineTwo}</LineTwo>
            </Box>
            <Flex position="absolute" right="0" height="100%" align="end">
                {props.details}
            </Flex>
        </Flex>
    );
};

export const SidebarLabelFallback: FC<
    SizeProps & {isHidden?: boolean}
> = props => (
    <ThreeDotsSpinner isHidden={props.isHidden}>
        <SidebarLabel
            icon={<></>}
            lineOne="Boo!"
            lineTwo="Why are you looking here?"
            size={props.size}
        />
    </ThreeDotsSpinner>
);
