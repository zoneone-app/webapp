import React, {FC, useState} from "react";
import {Search} from "./Search";

export interface RootSearchProps {
    onSubmit(value: string): void;
}

export const RootSearch: FC<RootSearchProps> = props => {
    const [value, setValue] = useState("");

    return (
        <Search
            value={value}
            activateChar="/"
            onChange={setValue}
            onSubmit={() => props.onSubmit(value)}
        />
    );
};
