import {Flex} from "@chakra-ui/react";
import React from "react";
import {Logo} from "./Logo";
import {Link} from "./Link";

export const Navigation = () => (
    <Flex as="nav" align="center">
        <Link href="/">
            <Logo size="lg" withLabel />
        </Link>
    </Flex>
);
