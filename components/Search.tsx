import {
    Center,
    Input,
    InputGroup,
    InputLeftElement,
    InputRightElement,
    Kbd
} from "@chakra-ui/react";
import React, {
    FC,
    KeyboardEventHandler,
    useCallback,
    useEffect,
    useRef
} from "react";
import {SearchIcon} from "@chakra-ui/icons";

interface SearchProps {
    activateChar?: string;
    value: string;

    onChange(value: string): void;

    onSubmit(): void;
}

export const Search: FC<SearchProps> = props => {
    const ref = useRef<HTMLInputElement>();

    // focus element when activation char is pressed anywhere
    useEffect(() => {
        if (!props.activateChar) return;

        const el = ref.current;
        if (!el) return;

        const handle = (e: KeyboardEvent) => {
            if (e.target === el) return;
            if (e.key !== props.activateChar) return;
            e.preventDefault();

            el.focus();
        };

        addEventListener("keydown", handle);
        return () => removeEventListener("keyup", handle);
    }, [props.activateChar, ref.current]);

    const handleInputBlur = useCallback<KeyboardEventHandler>(
        e => {
            if (e.key !== "Escape") return;

            const el = ref.current;
            if (!el) return;

            e.preventDefault();
            el.blur();
        },
        [ref.current]
    );

    const handleChange = useCallback(() => {
        const el = ref.current;
        if (!el) return;

        props.onChange(el.value);
    }, [ref.current, props.onChange]);

    const handleSubmit = useCallback(() => {
        props.onSubmit();
    }, [props.onSubmit]);

    return (
        <Center height="100%" ml={2} mr={2}>
            <InputGroup>
                <InputLeftElement pointerEvents="none">
                    <SearchIcon color="gray.400" />
                </InputLeftElement>

                <Input
                    type="search"
                    ref={ref}
                    value={props.value}
                    onChange={handleChange}
                    onSubmit={handleSubmit}
                    onKeyDown={handleInputBlur}
                />

                {props.activateChar && (
                    <InputRightElement pointerEvents="none">
                        <Kbd>{props.activateChar}</Kbd>
                    </InputRightElement>
                )}
            </InputGroup>
        </Center>
    );
};
