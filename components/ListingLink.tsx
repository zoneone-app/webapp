import styled from "@emotion/styled";
import React, {FC} from "react";
import {useColorModeValue} from "@chakra-ui/react";
import {Link, LinkProps} from "./Link";

const ListingLinkBase = styled(Link)`
    display: block;

    &.in-dark-theme {
        --hover-colour: #fff1;
        --active-colour: #fff2;
    }

    &:not(.in-dark-theme) {
        --hover-colour: #0001;
        --active-colour: #0001;
    }

    &:hover {
        background: var(--hover-colour);
        text-decoration: none;
    }

    &:active {
        background: var(--active-colour);
    }
`;

export const ListingLink: FC<LinkProps> = props => {
    const isDark = useColorModeValue(false, true);

    return <ListingLinkBase className={isDark && "in-dark-theme"} {...props} />;
};
