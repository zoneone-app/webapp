import "mapbox-gl/dist/mapbox-gl.css";
import Mapbox, {ZoomControl} from "react-mapbox-gl";
import React, {FC, ReactElement, useCallback, useMemo} from "react";
import {useColorModeValue} from "@chakra-ui/react";
import {LngLat, LngLatBounds} from "mapbox-gl";

const MAP_DARK_THEME = process.env.NEXT_PUBLIC_MAP_DARK_THEME;
const MAP_LIGHT_THEME = process.env.NEXT_PUBLIC_MAP_LIGHT_THEME;

const DEFAULT_LATITUDE = parseFloat(process.env.NEXT_PUBLIC_MAP_DEF_LAT);
const DEFAULT_LONGITUDE = parseFloat(process.env.NEXT_PUBLIC_MAP_DEF_LON);
const DEFAULT_ZOOM = parseFloat(process.env.NEXT_PUBLIC_MAP_DEF_ZOOM);

const MAP_BOUNDS = [
    [151, -28.5, 18],
    [155, -24.5, 9]
];

function attributionLink(text: string, link: string) {
    return `<a href="${link}" target="_blank">${text}</a>`;
}

const MapboxMap = Mapbox({
    accessToken: process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN,

    minZoom: MAP_BOUNDS[1][2],
    maxZoom: MAP_BOUNDS[0][2],

    dragRotate: false,

    customAttribution: [
        attributionLink(
            "TransLink",
            "https://translink.com.au/about-translink/open-data"
        )
    ],

    hash: true
});

export interface MapProps {
    target?: [number, number];
    zoom?: number;
    className?: string;

    onViewportChange?(viewport: LngLatBounds, zoom: number): void;
}

export const Map: FC<MapProps> = props => {
    const mapStyle = useColorModeValue(MAP_LIGHT_THEME, MAP_DARK_THEME);

    const handleViewportChange = useCallback((map: mapboxgl.Map) => {
        const viewport = map.getBounds();
        props.onViewportChange?.(viewport, map.getZoom());
    }, []);

    const center = useMemo<[number, number]>(
        () => [DEFAULT_LONGITUDE, DEFAULT_LATITUDE],
        []
    );
    const zoom = useMemo<[number]>(() => [DEFAULT_ZOOM], []);

    return (
        <MapboxMap
            style={mapStyle}
            maxBounds={
                new LngLatBounds([
                    new LngLat(MAP_BOUNDS[0][0], MAP_BOUNDS[0][1]),
                    new LngLat(MAP_BOUNDS[1][0], MAP_BOUNDS[1][1])
                ])
            }
            center={center}
            zoom={zoom}
            containerStyle={{
                width: "100%",
                height: "100%"
            }}
            className={props.className}
            onMoveEnd={handleViewportChange}
            onResize={handleViewportChange}
        >
            <ZoomControl />

            {props.children as ReactElement}
        </MapboxMap>
    );
};
