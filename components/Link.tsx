import {UrlObject} from "url";
import {
    Link as ChakraLink,
    LinkProps as ChakraLinkProps
} from "@chakra-ui/react";
import NextLink, {LinkProps as NextLinkProps} from "next/link";
import React, {FC} from "react";

interface OtherLinkProps {
    href: string | UrlObject;
}

type PassedNextLinkProps = Omit<NextLinkProps, "passHref" | "as" | "href">;
type PassedChakraLinkProps = Omit<ChakraLinkProps, "href">;

export type LinkProps = PassedChakraLinkProps &
    PassedNextLinkProps &
    OtherLinkProps;

const externalRegex = /^\w+:\/\//;

export const Link: FC<LinkProps> = ({href, ...props}) => {
    const external =
        typeof href === "string" ? externalRegex.test(href) : !!href.hostname;

    const nextProps: (keyof PassedNextLinkProps)[] = [
        "replace",
        "scroll",
        "shallow",
        "prefetch",
        "locale"
    ];

    const chakraProps: ChakraLinkProps = Object.fromEntries(
        Object.entries(props).filter(
            ([k]) => !nextProps.includes(k as keyof PassedNextLinkProps)
        )
    );

    const nextOnlyProps = Object.fromEntries(
        Object.entries(props).filter(([k]) =>
            nextProps.includes(k as keyof PassedNextLinkProps)
        )
    ) as PassedNextLinkProps;

    if (external) {
        if (typeof href !== "string")
            throw new Error("External URLs must use a string href");

        return <ChakraLink {...chakraProps} href={href} />;
    }

    return (
        <NextLink {...nextOnlyProps} href={href} passHref={true}>
            <ChakraLink {...chakraProps} />
        </NextLink>
    );
};
