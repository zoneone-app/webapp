import React, {FC, ReactNode, useMemo} from "react";
import {Text} from "@chakra-ui/react";
import {format} from "date-fns";

export interface TimeDisplayProps {
    time: Date;
    endTime?: Date;
    approximate: boolean;

    preText?: ReactNode;
    postText?: ReactNode;
}

export const TimeDisplay: FC<TimeDisplayProps> = props => {
    const display = useMemo(() => format(props.time, "HH:mm"), [props.time]);
    const endDisplay = useMemo(
        () => props.endTime && format(props.endTime, "HH:mm"),
        [props.endTime]
    );

    const approxNote = props.approximate ? "~" : "";
    const approxProps = props.approximate ? {color: "gray.500"} : {};

    return (
        <Text textStyle="subheadline" {...approxProps}>
            {props.preText || ""}
            {approxNote}
            {display}
            {endDisplay ? `-${endDisplay}` : ""}
            {props.postText || ""}
        </Text>
    );
};
