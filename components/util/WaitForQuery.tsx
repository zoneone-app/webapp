import {ParsedUrlQuery} from "querystring";
import {useRouter} from "next/router";
import React, {
    createContext,
    FC,
    ReactElement,
    ReactNode,
    Suspense,
    useContext,
    useEffect,
    useMemo
} from "react";
import {createSuspenseTrigger} from "../../utils/createSuspenseTrigger";
import {Sus} from "../../utils/api/types";

function useQuerySus() {
    const {query, isReady} = useRouter();
    const [trigger, handler] = useMemo(
        () => createSuspenseTrigger<typeof query>(),
        []
    );

    useEffect(() => {
        if (isReady) trigger(query);
    }, [isReady, query]);

    return handler;
}

const WaitForQueryContext = createContext<ParsedUrlQuery>(null);

export const useQuery = () => useContext(WaitForQueryContext);

interface WaitForQueryInternalProps {
    query: Sus<ParsedUrlQuery>;
    children?: ReactNode;
}

const WaitForQueryInternal: FC<WaitForQueryInternalProps> = props => (
    <WaitForQueryContext.Provider value={props.query()}>
        {props.children}
    </WaitForQueryContext.Provider>
);

interface WaitForQueryChildHandlerProps {
    children(query: ParsedUrlQuery): ReactElement;
}

const WaitForQueryChildHandler = (props: WaitForQueryChildHandlerProps) => {
    const query = useQuery();

    return props.children(query);
};

interface WaitForQueryProps {
    children?: ReactNode | WaitForQueryChildHandlerProps["children"];

    /** A fallback react tree to show when a Suspense child (like React.lazy) suspends */
    fallback: NonNullable<ReactNode> | null;
}

export const WaitForQuery: FC<WaitForQueryProps> = props => {
    const query = useQuerySus();

    return (
        <Suspense fallback={props.fallback}>
            <WaitForQueryInternal query={query}>
                {typeof props.children === "function" ? (
                    <WaitForQueryChildHandler>
                        {
                            props.children as WaitForQueryChildHandlerProps["children"]
                        }
                    </WaitForQueryChildHandler>
                ) : (
                    props.children
                )}
            </WaitForQueryInternal>
        </Suspense>
    );
};
