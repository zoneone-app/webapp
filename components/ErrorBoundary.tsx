import React, {Component, ReactNode} from "react";

export default class ErrorBoundary extends Component<{children?: ReactNode}, {error?: Error}> {
    static getDerivedStateFromError(error: Error) {
        return {error};
    }

    constructor(props: {children?: ReactNode}) {
        super(props);
        this.state = {error: null};
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
        console.error("Got component error", error, errorInfo.componentStack);
    }

    render() {
        if (this.state.error)
            return (
                <p>
                    <strong>Error:</strong> {this.state.error.message}
                </p>
            );

        return this.props.children;
    }
}
