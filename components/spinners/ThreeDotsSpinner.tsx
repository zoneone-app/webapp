/** @jsx jsx */
import {css, jsx, keyframes, PropsOf} from "@emotion/react";
import {CSSProperties, FC, SVGProps} from "react";
import {Box, Center, chakra, useColorModeValue} from "@chakra-ui/react";

const dotAnim = keyframes`
    0% {
        animation-timing-function: cubic-bezier(0.61, 0, 0.86, 0.43);
        opacity: 0;
        transform: translateX(-50px) scale(0);
    }

    40% {
        animation-timing-function: cubic-bezier(0.45, 0.31, 0.51, 0.69);
        transform: translateX(-30px) scale(1);
        opacity: 1;
    }

    60% {
        animation-timing-function: cubic-bezier(0.14, 0.57, 0.4, 1);
        transform: translateX(30px) scale(1);
        opacity: 1;
    }

    100% {
        opacity: 0;
        transform: translateX(50px) scale(0);
    }
`;

const dotStyle = css`
    opacity: 0;
    transform-origin: center;
    animation: linear ${dotAnim} infinite;
`;

const ActualSpinner = chakra(
    (
        props: SVGProps<SVGSVGElement> & {
            timeOffset?: number;
            duration?: number;
            count?: number;
        }
    ) => {
        const count = props.count || 12;
        const timeOffset = props.timeOffset || 0;
        const duration = props.duration || 4000;

        const times = Array.from(
            {length: count},
            (_, i) => timeOffset - i * (duration / count)
        );

        return (
            <svg
                width={props.width || (!props.height && 200)}
                height={props.height || (!props.width && 8)}
                viewBox="0 0 100 4"
                {...props}
            >
                {times.map((time, i) => (
                    <circle
                        key={i}
                        css={dotStyle}
                        fill="currentColor"
                        cx={50}
                        cy={2}
                        r={2}
                        style={{
                            animationDelay: time + "ms",
                            animationDuration: duration + "ms"
                        }}
                    />
                ))}
            </svg>
        );
    }
);

const showChildrenLoaderKeyframes = keyframes`
    0% {
        opacity: 1;
    }
    50% {
        opacity: 0;
    }
    100% {
        opacity: 0;
    }
`;

const showChildrenChildKeyframes = keyframes`
    0% {
        opacity: 0;
    }
    50% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
`;

const showChildrenStyle = css`
    .spinner-children-container {
        visibility: visible;
        animation: linear 500ms ${showChildrenChildKeyframes};
    }

    .spinner-spinner-container {
        opacity: 0;
        pointer-events: none;
        animation: linear 500ms ${showChildrenLoaderKeyframes};
    }
`;

interface SpinnerProps {
    isLoading?: boolean;
    isHidden?: boolean;

    colour?: string;

    spinner?: PropsOf<typeof ActualSpinner>;

    className?: string;
    style?: CSSProperties;
}

const Spinner: FC<SpinnerProps> = props => {
    const defaultColour = useColorModeValue("gray.600", "gray.600");
    const colour = props.colour || defaultColour;
    const isLoading = props.isLoading == null ? true : props.isLoading;

    return (
        <Box
            position="relative"
            className={props.className}
            css={!isLoading && showChildrenStyle}
            style={props.style}
        >
            <Box visibility="hidden" className="spinner-children-container">
                {props.children}
            </Box>

            <Center
                position="absolute"
                style={{top: 0, left: 0, bottom: 0, right: 0}}
                visibility={props.isHidden ? "hidden" : "visible"}
                pointerEvents={props.isHidden ? "none" : "auto"}
                className="spinner-spinner-container"
            >
                <ActualSpinner
                    alt="Loading..."
                    height={3}
                    color={colour}
                    {...props.spinner}
                />
            </Center>
        </Box>
    );
};

export const ThreeDotsSpinner = chakra(Spinner);
