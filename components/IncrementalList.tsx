import React, {ReactElement, useEffect, useMemo, useRef, useState} from "react";

export interface IncrementalListProps<T> {
    items: T[];
    children(item: T, index: number): ReactElement;

    interval?: number;
    countPerInterval?: number;
    hasChanged?(item: T): boolean;
}

export function IncrementalList<T>(props: IncrementalListProps<T>) {
    const itemCount = useRef(0);
    const itemMap: Map<number, ReactElement> = useMemo(() => new Map(), []);
    const [, render] = useState({});

    useEffect(() => {
        const timeout = setInterval(() => {
            const newItemCount = Math.min(
                itemCount.current + (props.countPerInterval || 10),
                props.items.length
            );

            let updated = false;
            for (let i = 0; i < newItemCount; i++) {
                const item = props.items[i];
                if (itemMap.has(i) && props.hasChanged?.(item)) continue;

                updated = true;
                itemMap.set(i, props.children(item, i));
            }

            itemCount.current = newItemCount;
            if (newItemCount >= props.items.length) clearInterval(timeout);

            if (updated) render({});
        }, props.interval || 100);

        return () => clearInterval(timeout);
    }, [props.items]);

    return <>{Array.from(itemMap.values())}</>;
}
