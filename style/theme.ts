import {extendTheme} from "@chakra-ui/react";
import "@fontsource/fira-sans";
import "@fontsource/fira-sans/700.css";
import "@fontsource/fira-sans/800.css";
import "@fontsource/roboto";
import {style as Logo} from "../components/Logo";
import {style as RouteBullet} from "../components/RouteBullet";

export const theme = extendTheme({
    components: {
        Logo,
        RouteBullet
    },

    colors: {
        // https://material.io/inline-tools/color/
        brand: {
            50: "#ffebed",
            100: "#ffcdd0",
            200: "#f09b96",
            300: "#e6756d",
            400: "#ef5648",
            500: "#f3492b",
            600: "#e43f2b",
            700: "#d33525",
            800: "#c62e1e",
            900: "#b72311"
        },
        matLightGreen: {
            50: "#f1f8e9",
            100: "#dcedc8",
            200: "#c5e1a5",
            300: "#aed581",
            400: "#9ccc65",
            500: "#8bc34a",
            600: "#7cb342",
            700: "#689f38",
            800: "#689f38",
            900: "#689f38"
        },
        matGreen: {
            50: "#e8f5e9",
            100: "#c8e6c9",
            200: "#a5d6a7",
            300: "#81c784",
            400: "#66bb6a",
            500: "#4caf50",
            600: "#43a047",
            700: "#388e3c",
            800: "#2e7d32",
            900: "#1b5e20"
        }
    },

    fonts: {
        heading: "Fira Sans",
        body: "Roboto"
    },

    textStyles: {
        "routes/number-lg": {
            fontWeight: "800",
            fontSize: "1.625rem",
            lineHeight: "121%"
        },
        "routes/prefix-lg": {
            fontWeight: "600",
            fontSize: "1.4rem",
            lineHeight: "121%"
        },
        "routes/number-md": {
            fontWeight: "800",
            fontSize: "1rem",
            lineHeight: "121%"
        },
        "routes/prefix-md": {
            fontWeight: "600",
            fontSize: "0.875rem",
            lineHeight: "121%"
        },
        "routes/number-sm": {
            fontWeight: "800",
            fontSize: "0.75rem",
            lineHeight: "121%"
        },
        "routes/prefix-sm": {
            fontWeight: "600",
            fontSize: "0.625rem",
            lineHeight: "121%"
        },
        "title-xl": {
            fontSize: "2.125rem",
            lineHeight: "121%",
            fontWeight: "bold"
        },
        "title-lg": {
            fontSize: "1.75rem",
            lineHeight: "121%",
            fontWeight: "normal"
        },
        "title-md": {
            fontSize: "1.375rem",
            lineHeight: "121%",
            fontWeight: "bold"
        },
        "title-md-normal": {
            fontSize: "1.375rem",
            lineHeight: "121%",
            fontWeight: "normal"
        },
        "title-sm": {
            fontSize: "1.25rem",
            lineHeight: "121%",
            fontWeight: "normal"
        },
        headline: {
            fontSize: "1.0625rem",
            lineHeight: "121%",
            fontWeight: "bold"
        },
        body: {
            fontSize: "1.0625rem",
            lineHeight: "121%"
        },
        "body-bold": {
            fontSize: "1.0625rem",
            lineHeight: "121%",
            fontWeight: "bold"
        },
        callout: {
            fontSize: "1rem",
            lineHeight: "121%"
        },
        subheadline: {
            fontSize: "0.9375rem",
            lineHeight: "121%"
        },
        footnote: {
            fontSize: "0.8125rem",
            lineHeight: "121%"
        },
        caption: {
            fontSize: "0.75rem",
            lineHeight: "121%"
        },
        "caption-sm": {
            fontSize: "0.6875rem",
            lineHeight: "121%"
        }
    },

    config: {
        useSystemColorMode: true
    }
});
