const withImage = require("next-images");

module.exports = withImage({
    target: "serverless",
    reactStrictMode: true,
    future: {
        webpack5: true
    },
    experimental: {
        reactMode: "concurrent"
    }
});
