import {useMemo} from "react";
import {createSuspenseTrigger} from "./createSuspenseTrigger";

export function useSuspenseTrigger<T>() {
    return useMemo(() => createSuspenseTrigger<T>(), []);
}
