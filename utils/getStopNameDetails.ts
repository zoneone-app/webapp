import {SidebarLabelLines} from "../components/sidebar/heading/SidebarLabel";

const stopNameRegex = /^(?<street>[\w ]+?)(?:(?:[(,-]? *)?(?:(?:stop|stand|zone) )?(?<stopNo1>[\d/]+\w?))? (?:(?<relation>at|near|nr|app|on|opp) (?<location>[\w' -]+?)(?: near (?<near>[\w ]+))? ?)?(?:(?:[(,-]? *)?(?:(?:stop|stand|zone) )?(?<stopNo2>[\d/]+\w?))?(?: (?:(?:hail|park) ?'n' ?ride|\(\w+\)|closed .*))?$/i;
const simpleStopRegex = /^(?<location>[\w' -]+) *[,-] *(?<street>[\w' -]+)$/i;

function getExtremelyBasicStopNameDetails(
    name: string,
    suburb: string
): SidebarLabelLines {
    return {
        lineOne: name,
        lineTwo: suburb
    };
}

function getBasicStopNameDetails(
    name: string,
    suburb: string
): SidebarLabelLines {
    const match = name.match(simpleStopRegex);
    if (match == null) return getExtremelyBasicStopNameDetails(name, suburb);

    const {location, street} = match.groups;

    const suburbLabel = suburb ? ` (${suburb})` : "";

    return {
        lineOne: location,
        lineTwo: `${street}${suburbLabel}`
    };
}

const relationMaps = {
    nada: "",
    at: "at",
    near: "near",
    nr: "near",
    opp: "opposite"
};

export function getStopNameDetails(
    name: string,
    suburb?: string
): SidebarLabelLines {
    const match = name.match(stopNameRegex);
    if (match == null) return getBasicStopNameDetails(name, suburb);

    const {street, stopNo1, relation, location, near, stopNo2} = match.groups;
    const stopNo = stopNo1 || stopNo2;
    const relationLower = (relation?.toLowerCase() ??
        "nada") as keyof typeof relationMaps;

    const nearLabel = near ? ` near ${near}` : "";
    const suburbLabel = suburb ? ` (${suburb})` : "";
    const stopLabel = stopNo ? `, stop ${stopNo}` : "";
    const relationLabel =
        relationLower === "at" || relationLower === "nada"
            ? ""
            : `${relationMaps[relationLower]} `;

    const locationValue = location ?? "";

    const l1Label = relationLower === "at" ? locationValue : street;
    const l2Label = relationLower === "at" ? street : locationValue;

    return {
        lineOne: `${l1Label}${stopLabel}`,
        lineTwo: `${relationLabel}${l2Label}${nearLabel}${suburbLabel}`
    };
}
