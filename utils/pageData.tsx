import React, {
    createContext,
    FC,
    memo,
    ReactElement,
    ReactNode,
    Suspense,
    unstable_useTransition as useTransition,
    useContext,
    useEffect,
    useMemo,
    useState
} from "react";
import {SusRes, Triggerable} from "./api/types";
import Stop from "./api/Stop";
import Trip from "./api/Trip";
import {instantSus, isTriggerable, susFromPromise} from "./api/utils";
import noop from "./noop";
import Importer from "./api/dashboard/Importer";
import {useSuspenseTrigger} from "./useSuspenseTrigger";

interface IdAndTriggerable {
    id: string;
    value: Triggerable;
}

interface AllTriggerable {
    [key: string]: Triggerable | IdAndTriggerable;
}

export interface PageData extends AllTriggerable {
    stop?: {id: string; value: Stop};
    trip?: {id: string; value: Trip};
    dashImporter?: Importer;

    mapGeoJson?: SusRes<GeoJSON.Feature>;
}

type SetPendingFunc = (state: boolean) => void;
type SetPageDataFunc = (newData: PageData) => void;
type SetPageDataCb = () => PageData;
type SetPageDataCbFunc = (cb: SetPageDataCb) => void;
type SetPageDataCbFuncInternal = (cb: SetPageDataCb) => Promise<PageData>;

interface PageDataContext {
    data: PageData;
    pending: boolean;
    setData: SetPageDataFunc;
    setPending: SetPendingFunc;
}

interface PageLoaderHandler {
    transition: ReturnType<typeof useTransition>[0];
    update: SetPageDataFunc;
}

interface UsePageLoaderResult {
    set: SetPageDataCbFunc;
    isPending: boolean;
}

const PageDataContext = createContext<PageDataContext>(null);
const usePageDataContext = () => useContext(PageDataContext);

PageDataContext.displayName = "PageDataContext";

function loadPageData(
    handler: PageLoaderHandler,
    newValue: () => PageData
): Promise<PageData> {
    return new Promise(yay => {
        // make sure it happens outside of all renders
        handler.transition(() => {
            const newVal = newValue();

            Object.values(newVal).forEach(value => {
                // trigger the api call
                if (isTriggerable(value)) value.trigger();
                else if (value.value) value.value.trigger();
            });

            handler.update(newVal);
            yay(newVal);
        });
    });
}

function usePageLoaderTransition() {
    // note, the typings appear to be backwards here
    const [isPending, transition] = useTransition({
        busyMinDurationMs: 2000,
        busyDelayMs: 500
    });
    return [
        (transition as unknown) as typeof isPending,
        (isPending as unknown) as typeof transition
    ] as const;
}

export function usePageLoader(): UsePageLoaderResult {
    const data = usePageDataContext();
    const [transition, isPending] = usePageLoaderTransition();

    useEffect(() => {
        data.setPending(isPending);
    }, [data.setPending, isPending]);

    const loadPageDataMethod = useMemo(
        () =>
            loadPageData.bind(loadPageData, {
                transition,
                update: data.setData
            }),
        [transition, data.setData]
    );

    return {
        set: loadPageDataMethod,
        isPending
    };
}

export function usePageData() {
    const context = usePageDataContext();
    if (!context)
        throw new Error("usePageData() must be inside the PageDataProvider");
    return context.data;
}

export function usePageLoading() {
    const {pending} = usePageDataContext();
    return pending;
}

interface PageDataConsumerWaiterProps<T> {
    children: ReactNode | ((data: PageData) => ReactElement);
    map(src: PageData): T;
    isExisting(val: T): boolean;
}

function PageDataConsumerWaiter<T>(props: PageDataConsumerWaiterProps<T>) {
    const data = usePageData();
    const [trigger, suspense] = useSuspenseTrigger<void>();

    if (props.isExisting(props.map(data))) {
        trigger();
    }

    suspense();

    if (typeof props.children === "function") {
        return props.children(data);
    } else {
        return props.children;
    }
}

export interface PageDataConsumerProps<T, LoadParams extends unknown[]> {
    map(src: PageData): T;
    load(...params: LoadParams): PageData;
    isExisting?(val: T): boolean;
    children: ReactNode | ((data: PageData) => ReactElement);
    fallback: ReactNode;
    loadParams?: LoadParams;
}

export function PageDataConsumer<T, LoadParams extends unknown[]>(
    props: PageDataConsumerProps<T, LoadParams>
) {
    const isExisting = props.isExisting ?? (v => !!v);
    const {map, load} = props;

    const previousPageData = usePageData();
    const previousMapped = map(previousPageData);

    useDefaultPageData(
        isExisting(previousMapped),
        previousPageData,
        load.bind(null, ...(props.loadParams ?? []))
    );

    return (
        <Suspense fallback={props.fallback}>
            <PageDataConsumerWaiter map={map} isExisting={isExisting}>
                {props.children}
            </PageDataConsumerWaiter>
        </Suspense>
    );
}

export function useDefaultPageData(
    useExisting: boolean,
    existing: PageData,
    def: SetPageDataCb
): SusRes<PageData> {
    const loader = usePageLoader();

    const {promise, trigger} = useMemo(() => {
        let trigger: () => void = noop;

        const promise = new Promise<PageData>((yay, nay) => {
            let triggered = false;
            trigger = () => {
                if (triggered) return;
                triggered = true;

                const setPromise = (loader.set as SetPageDataCbFuncInternal)(
                    def
                );
                setPromise.then(yay).catch(nay);
            };
        });

        return {promise, trigger};
    }, [loader.set]);

    useEffect(() => {
        if (useExisting) return;
        trigger();
    }, [useExisting, trigger]);

    const promiseSus = useMemo(() => susFromPromise(promise), [promise]);

    if (useExisting) {
        return instantSus(
            existing,
            isTriggerable(existing) ? existing.trigger : noop
        );
    }

    return promiseSus;
}

export const PageDataProvider: FC = props => {
    const [data, setData] = useState<PageData>({});
    const [pending, setPending] = useState(0);

    const contextValue: PageDataContext = {
        data,
        pending: pending > 0,
        setData,
        setPending: v => setPending(Math.max(0, v ? pending + 1 : pending - 1))
    };

    return (
        <PageDataContext.Provider value={contextValue}>
            {props.children}
        </PageDataContext.Provider>
    );
};
