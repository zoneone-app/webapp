import noop from "./noop";

export type SusTrig<T> = [(value: T) => void, () => T];

type ResolveFunction<T> = (v: T) => void;

export function createSuspenseTrigger<T>(): SusTrig<T> {
    let resolve: ResolveFunction<T> = noop,
        resolved = false,
        value: T = null;

    const promise = new Promise<T>(yay => {
        resolve = (val: T) => {
            resolved = true;
            value = val;
            yay(value);
        };
    });

    return [
        resolve,
        () => {
            if (!resolved) throw promise;
            return value;
        }
    ];
}
