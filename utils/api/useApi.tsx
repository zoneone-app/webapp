import React, {
    createContext,
    FC,
    useCallback,
    useContext,
    useEffect,
    useMemo,
    useRef
} from "react";
import {useAuth0} from "@auth0/auth0-react";
import {Root} from "./Root";
import {ApiAuth} from "./Api";

interface ApiContext {
    getApi(): Root;
}

const baseApiContext: ApiContext = {
    getApi() {
        throw new Error("Component is not wrapped in <ApiProvider />");
    }
};

const ApiContext = createContext<ApiContext>(baseApiContext);
ApiContext.displayName = "ApiContext";

function useApiAuth(audience: string): ApiAuth {
    const auth0 = useAuth0();

    const auth0Ref = useRef(auth0);

    useEffect(() => {
        auth0Ref.current = auth0;
    }, [auth0Ref, auth0]);

    useEffect(() => {
        if (auth0Ref.current.error) throw auth0Ref.current.error;
    }, [auth0Ref.current]);

    const getAccessToken = useCallback<ApiAuth["getAccessToken"]>(
        async (scopes: string[]) => {
            const {
                isAuthenticated,
                loginWithPopup,
                getAccessTokenSilently
            } = auth0Ref.current;

            const scope = scopes.join(" ");
            if (!isAuthenticated) {
                console.warn("Not logged in, redirecting to login page");
                await loginWithPopup({
                    audience,
                    scope
                });
            }

            return getAccessTokenSilently({
                audience,
                scope
            });
        },
        [auth0Ref]
    );

    return {
        getAccessToken
    };
}

export const ApiProvider: FC<{baseUrl: string; audience: string}> = props => {
    const apiAuth = useApiAuth(props.audience);

    const api = useMemo(() => new Root(props.baseUrl, apiAuth), [
        props.baseUrl,
        apiAuth
    ]);
    const getApi = useCallback(() => api, [api]);

    return (
        <ApiContext.Provider value={{getApi}}>
            {props.children}
        </ApiContext.Provider>
    );
};

export const useApi = () => {
    const api = useContext(ApiContext);
    return api.getApi();
};
