import {getEpochFromDate} from "../date";
import {SusRes, Triggerable} from "./types";
import StopTrip, {StopTripInfo} from "./StopTrip";
import {Api, InitTransparent} from "./Api";
import {Root} from "./Root";

export interface StopInfo {
    id: string;
    gtfsId: number;
    name: string;
    suburb?: string;
    zone: number;
    otherZone?: number;
    isDummy: boolean;
    stationId?: string;
}

interface EagerRequests {
    trips?: boolean;
}

export default class Stop extends Api<Root> implements Triggerable {
    private readonly stopInfo: SusRes<StopInfo>;

    private today = getEpochFromDate(new Date());
    private tripCache: Map<number, SusRes<{trips: StopTripInfo[]}>> = new Map();

    constructor(
        init: InitTransparent,
        root: Root,
        id: string,
        stopInfo: SusRes<StopInfo>,
        signal?: AbortSignal,
        eagerRequests?: EagerRequests
    ) {
        super(init, root, {id});
        this.stopInfo = stopInfo;

        // eagerly load subvalues
        if (eagerRequests?.trips !== false) this.trips(new Date(), signal);
    }

    trigger(highPriority?: boolean) {
        this.stopInfo.trigger(highPriority);
        this.tripCache.get(this.today)?.trigger(true);
    }

    get id() {
        return this.autoSusEx("id", this.stopInfo);
    }

    get gtfsId() {
        return this.autoSusEx("gtfsId", this.stopInfo);
    }

    get stationId() {
        return this.autoSusEx("stationId", this.stopInfo);
    }

    get name() {
        return this.autoSusEx("name", this.stopInfo);
    }

    get suburb() {
        return this.autoSusEx("suburb", this.stopInfo);
    }

    get zone() {
        return this.autoSusEx("zone", this.stopInfo);
    }

    get otherZone() {
        return this.extendSus("otherZone", this.stopInfo, v =>
            v.otherZone === 0 ? null : v.otherZone
        );
    }

    get isDummy() {
        return this.autoSusEx("isDummy", this.stopInfo);
    }

    getGtfsId(pad = false) {
        return this.extendSus("gtfsIdPadded", this.gtfsId, v =>
            pad ? v.toString().padStart(6, "0") : v.toString()
        );
    }

    trips(date: Date, signal?: AbortSignal) {
        const epochDate = getEpochFromDate(date);

        const sus = this.tripCache.has(epochDate)
            ? this.tripCache.get(epochDate)
            : this.req<{trips: StopTripInfo[]}>(
                  "Stops:GetStopTrips",
                  {day: epochDate.toString()},
                  signal
              );

        const trips = this.extendSus(
            "trips",
            sus,
            v => v.trips.map(trip => new StopTrip(this.init, this.root, trip)),
            v => v.map(el => el.trigger())
        );

        if (!this.tripCache.has(epochDate)) this.tripCache.set(epochDate, sus);
        return trips;
    }
}

export class Stops extends Api<Root> {
    private stopCache: Map<string, Stop> = new Map();

    constructor(init: InitTransparent, root: Root) {
        super(init, root);
    }

    getById(id: string, signal?: AbortSignal, eagerRequests?: EagerRequests) {
        if (this.stopCache.has(id)) return this.stopCache.get(id);

        const stop = new Stop(
            this.init,
            this.root,
            id,
            this.req<StopInfo>("Stops:GetStop", {id}, signal),
            signal,
            eagerRequests
        );

        this.stopCache.set(id, stop);
        return stop;
    }
}
