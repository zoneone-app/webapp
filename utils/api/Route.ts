import {SusRes, Triggerable} from "./types";
import {Api, InitTransparent} from "./Api";
import {getReqValRes} from "./utils";
import {Root} from "./Root";

export enum RouteBranding {
    None,
    Buz,
    CityGlider,
    Rocket,
    CityPrecincts,
    Bullet,
    NightLink
}

export enum RouteDirection {
    TwoWay,
    InvTwoWay,
    Primary,
    Secondary
}

export enum RouteDirectionType {
    InOut,
    NorthSouth,
    EastWest,
    Clock,
    Stream
}

export enum RouteMode {
    Bus,
    Train,
    Citycat,
    CityHopper,
    CityFerry,
    Ferry,
    GLink,
    RailBus
}

export enum RouteStoppingPatternType {
    AllStops,
    Local,
    LocalExpress,
    LimitedStops,
    CityXpress,
    Busway,
    LimitedExpress,
    Express
}

export enum RouteStyling {
    BusExpress,
    BusStandard,
    CityGliderBlue,
    CityGliderMaroon,
    CityHopper,
    CityLoop,
    Ferry,
    Glink,
    GoldCoastExpress,
    MtCoottha,
    NightLink,
    SpringHill,
    TfbBuz,
    TfbExpress,
    TfbStandard,
    Train,
    TrainDarkBlue,
    TrainGreen,
    TrainGrey,
    TrainLightBlue,
    TrainPurple,
    TrainRed,
    TrainYellow,
    StationLink
}

export interface RouteInfo {
    id: string;
    code: string;
    title: string;
    description: string;
    mode: RouteMode;
    styling: RouteStyling;
    directionType: RouteDirectionType;
    direction: RouteDirection;
    branding: RouteBranding;
    stoppingPatternType: RouteStoppingPatternType;
}

export default class Route extends Api<Root> implements Triggerable {
    private readonly routeInfo: SusRes<RouteInfo>;

    constructor(
        init: InitTransparent,
        root: Root,
        id: string,
        routeInfo: SusRes<RouteInfo>
    ) {
        super(init, root, {id});
        this.routeInfo = routeInfo;
    }

    trigger(highPriority?: boolean) {
        this.logTrigger();
        this.routeInfo.trigger(highPriority);
    }

    isLoading() {
        return this.routeInfo.loading;
    }

    get id() {
        return this.autoSusEx("id", this.routeInfo);
    }
    get code() {
        return this.autoSusEx("code", this.routeInfo);
    }
    get title() {
        return this.autoSusEx("title", this.routeInfo);
    }
    get description() {
        return this.autoSusEx("description", this.routeInfo);
    }
    get mode() {
        return this.autoSusEx("mode", this.routeInfo);
    }
    get styling() {
        return this.autoSusEx("styling", this.routeInfo);
    }
    get directionType() {
        return this.autoSusEx("directionType", this.routeInfo);
    }
    get direction() {
        return this.autoSusEx("direction", this.routeInfo);
    }
    get branding() {
        return this.autoSusEx("branding", this.routeInfo);
    }
    get stoppingPatternType() {
        return this.autoSusEx("stoppingPatternType", this.routeInfo);
    }
}

export class Routes extends Api<Root> {
    private routeCache: Map<string, Route> = new Map();

    constructor(init: InitTransparent, root: Root) {
        super(init, root);
    }

    getById(id: string, signal?: AbortSignal) {
        const actualId = getReqValRes(id);

        if (this.routeCache.has(actualId)) return this.routeCache.get(actualId);

        const route = new Route(
            this.init,
            this.root,
            id,
            this.req<RouteInfo>("Routes:GetRoute", {id}, signal)
        );

        this.routeCache.set(actualId, route);
        return route;
    }
}
