import {Api, ApiAuth} from "./Api";
import {Routes} from "./Route";
import {Stops} from "./Stop";
import {StoppingPatterns} from "./StoppingPattern";
import {Trips} from "./Trip";
import {susFromPromise} from "./utils";
import {SusRes} from "./types";
import {Dashboard} from "./dashboard";

export class Root extends Api<null> {
    private _stops: Stops;
    private _routes: Routes;
    private _stoppingPatterns: StoppingPatterns;
    private _trips: Trips;
    private _dashboard: Dashboard;
    private _stopTileUrl: SusRes<string>;

    constructor(private base: string, auth: ApiAuth) {
        super(Api.loadInitData(base, auth), null);
    }

    getStopTileUrl() {
        if (this._stopTileUrl) return this._stopTileUrl;
        this._stopTileUrl = susFromPromise(
            this.getRouteTemplateUrl("Stops:GetMpbxvec")
        );
        return this._stopTileUrl;
    }

    stops(): Stops {
        if (this._stops) return this._stops;
        this._stops = new Stops(this.init, this);
        return this._stops;
    }

    routes(): Routes {
        if (this._routes) return this._routes;
        this._routes = new Routes(this.init, this);
        return this._routes;
    }

    stoppingPatterns(): StoppingPatterns {
        if (this._stoppingPatterns) return this._stoppingPatterns;
        this._stoppingPatterns = new StoppingPatterns(this.init, this);
        return this._stoppingPatterns;
    }

    trips(): Trips {
        if (this._trips) return this._trips;
        this._trips = new Trips(this.init, this);
        return this._trips;
    }

    dashboard(): Dashboard {
        if (this._dashboard) return this._dashboard;
        this._dashboard = new Dashboard(this.init, this);
        return this._dashboard;
    }
}
