export type ReqVal<T> = T extends () => unknown
    ? T | (() => T)
    // eslint-disable-next-line @typescript-eslint/ban-types
    : T extends Function
    ? never
    : T | (() => T);

export interface SusRes<T> {
    /**
     * Triggers this suspense to run.
     * @remarks Not always implemented
     * @param highPriority - Run this sus at a high priority
     */
    trigger: (highPriority?: boolean) => void;

    /**
     * A React Suspense function, returns the result value once completed
     */
    suspense: Sus<T>;

    /**
     * A promise that will resolve when the sus completes
     */
    promise: Promise<T>;

    // normal promise api, for times when speed is a concern
    /**
     * Becomes true once the sus has completed successfully
     */
    done: boolean;

    /**
     * True while the sus is running
     */
    loading: boolean;

    /**
     * True if the sus encountered an error
     */
    errored: boolean;

    /**
     * The value of the sus, if done == true
     */
    value?: T;

    // allow calling to give suspense too
    /**
     * @inheritDoc trigger
     */
    (): T;
}

export type Sus<T> = () => T;

export interface Triggerable {
    trigger(highPriority?: boolean): void;
}

export interface SusData<T> {
    promise: Promise<T>;
}
