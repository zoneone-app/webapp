import {Semaphore} from "async-mutex";
import {Path, PathValue} from "../Path";
import {ReqVal, SusRes} from "./types";
import {
    autoSusEx,
    extendSus,
    extendSusRes,
    getReqValRes,
    susFromPromise
} from "./utils";

interface Svm<T> {
    value: T;
    status: string[];
}

interface HttpErrorResponse {
    error: true;
    param: string;
    message: string;
}

interface Route {
    name: string;
    method: string;
    template: string;
    requiredScopes: string[];
    requiresAuth: boolean;
}

type RouteList = Route[];

interface InitData {
    baseUrl: string;
    routes: RouteList;
    auth: ApiAuth;
}

/**
 * A transparent type only used internally
 */
export type InitTransparent = Promise<InitData>;

type Query = Record<string, string>;

function isError(val: unknown): val is HttpErrorResponse {
    return val && (val as {error: unknown}).error === true;
}

function createErrorMessage(value: HttpErrorResponse) {
    return `(${value.param}): ${value.message}`;
}

export interface ApiAuth {
    getAccessToken(scopes: string[]): Promise<string>;
}

type FormDataDict = Record<string, string | Blob>;
type BodyType =
    | string
    | Blob
    | FormData
    | URLSearchParams
    | ReadableStream
    | FormDataDict;

export class Api<TRoot> {
    private static readonly triggerLogEnabled = false;
    private static readonly logQueues = false;
    private static readonly querySemaphore = new Semaphore(10);
    private static readonly templateRegex = /{(\w+)}/g;
    private readonly susCache: Map<string, SusRes<unknown>> = new Map();

    /**
     * @param init - Initialisation data. Use parent api `this.init` field or `Api.loadInitData()` in root.
     * @param root - The root api, so that you can use it in your code without a global variable
     * @param baseQuery - Query values to be applied to every request
     */
    public constructor(
        protected readonly init: InitTransparent,
        protected readonly root: TRoot,
        private readonly baseQuery: Query = {}
    ) {}

    private static async loadInitDataImpl(
        baseUrl: string,
        auth: ApiAuth
    ): Promise<InitData> {
        const url = new URL(".well-known/routes", baseUrl);

        const routes = await fetch(url.toString())
            .then(res => res.json())
            .then(res => res.value || []);

        return {
            baseUrl,
            routes,
            auth
        };
    }

    public static loadInitData(
        baseUrl: string,
        auth: ApiAuth
    ): InitTransparent {
        return this.loadInitDataImpl(baseUrl, auth);
    }

    public static readonly defaultQuery = () => ({});

    private static fillTemplatePath(template: string, query: Query) {
        const excludedKeys: string[] = [];

        const resultUrl = template.replace(this.templateRegex, (v, name) => {
            if (query[name]) {
                excludedKeys.push(name);
                return query[name];
            } else {
                return v;
            }
        });

        return {
            path: resultUrl,
            queryValues: Object.fromEntries(
                Object.entries(query).filter(
                    ([key]) => !excludedKeys.includes(key)
                )
            )
        };
    }

    private static createFormData(source: FormDataDict) {
        const formData = new FormData();

        for (const [key, value] of Object.entries(source)) {
            formData.set(key, value);
        }

        return formData;
    }

    private getRoute(routes: RouteList, endpointName: string) {
        const route = routes.find(v => v.name === endpointName);
        if (!route) throw new Error(`Invalid endpoint ${endpointName}`);
        return route;
    }

    async getRouteTemplateUrl(endpointName: string) {
        const {routes, baseUrl} = await this.init;
        const route = this.getRoute(routes, endpointName);
        const templateUrl = new URL(route.template, baseUrl);
        return decodeURI(templateUrl.toString());
    }

    async fillRouteUrl(endpointName: string, query: Query) {
        const mergedQuery = {...this.baseQuery, ...query};

        const {routes, baseUrl} = await this.init;
        const route = this.getRoute(routes, endpointName);

        const {path: actualPath, queryValues} = Api.fillTemplatePath(
            route.template,
            mergedQuery
        );

        const url = new URL(actualPath, baseUrl);

        for (const [key, value] of Object.entries(queryValues)) {
            url.searchParams.append(key, value);
        }

        return url.toString();
    }

    async getEndpointAuthentication(name: string) {
        const {routes} = await this.init;
        const route = this.getRoute(routes, name);
        return {required: route.requiresAuth, scopes: route.requiredScopes};
    }

    async getEndpointMethod(endpointName: string) {
        const {routes} = await this.init;
        const route = this.getRoute(routes, endpointName);
        return route.method;
    }

    protected logTrigger() {
        if (process.env.NODE_ENV === "production" || !Api.triggerLogEnabled)
            return;

        const name = this.constructor.name;
        console.debug("Trigger", name);
    }

    // Wraps extendSus in a cache
    protected extendSus<TV, TR>(
        key: string,
        src: SusRes<TV>,
        map: (val: TV) => TR,
        trigger?: (val: TR) => void
    ): SusRes<TR> {
        if (this.susCache.has(key)) return this.susCache.get(key) as SusRes<TR>;
        const res = extendSus(src, map, trigger);
        this.susCache.set(key, res);
        return res;
    }

    // Wraps autoSusEx in a cache
    protected autoSusEx<TV, TP extends Path<TV>>(
        path: TP,
        src: SusRes<TV>,
        triggerFn?: (val: PathValue<TV, TP>) => void
    ): SusRes<PathValue<TV, TP>> {
        type Value = PathValue<TV, TP>;

        if (this.susCache.has(path as string))
            return this.susCache.get(path as string) as SusRes<Value>;
        const res = autoSusEx<TV, TP>(path, src, triggerFn);
        this.susCache.set(path as string, res);
        return res;
    }

    protected extendSusRes<TV, TR>(
        key: string,
        src: SusRes<TV>,
        map: (val: TV) => SusRes<TR>
    ): SusRes<TR> {
        if (this.susCache.has(key)) return this.susCache.get(key) as SusRes<TR>;
        const res = extendSusRes(src, map);
        this.susCache.set(key, res);
        return res;
    }

    private async getAccessToken(scopes: string[]) {
        const {auth} = await this.init;
        return auth.getAccessToken(scopes);
    }

    private async reqPromise<T>(
        setTrigger: (trig: (highPriority?: boolean) => void) => void,
        endpointName: string,
        query: ReqVal<Query>,
        body?: BodyType,
        signal?: AbortSignal
    ): Promise<T> {
        let triggered = false,
            highPriority = false;
        await new Promise<void>(yay => {
            setTrigger(setHighPriority => {
                if (triggered) return;
                triggered = true;
                highPriority = setHighPriority;
                yay();
            });
        });

        const actualQuery = getReqValRes(query);
        const absolutePath = await this.fillRouteUrl(endpointName, actualQuery);
        const method = await this.getEndpointMethod(endpointName);

        const {
            required: requiresAuth,
            scopes
        } = await this.getEndpointAuthentication(endpointName);

        if (process.env.NODE_ENV !== "production" && Api.logQueues)
            console.debug("Running:", method, absolutePath);

        const accessToken = requiresAuth
            ? await this.getAccessToken(scopes)
            : null;

        const headers = new Map<string, string>();

        if (accessToken) {
            headers.set("Authorization", `Bearer ${accessToken}`);
        }

        const bodyValue =
            typeof body !== "object" ||
            body instanceof Blob ||
            body instanceof FormData ||
            body instanceof URLSearchParams ||
            body instanceof ReadableStream
                ? body
                : Api.createFormData(body);

        const json = await Api.querySemaphore.runExclusive(async () => {
            const res = await fetch(absolutePath, {
                method,
                headers: Object.fromEntries(headers.entries()),
                body: bodyValue,
                credentials: accessToken ? "include" : "omit",
                signal
            });
            return (await res.json()) as Svm<T>;
        });

        if (json.status.length !== 0) throw new Error("Status is not empty");
        if (isError(json.value))
            throw new Error(createErrorMessage(json.value));
        return json.value;
    }

    protected req<T>(
        endpointName: string,
        query: ReqVal<Query>,
        bodyOrSignal?: BodyType | AbortSignal,
        signal?: AbortSignal
    ): SusRes<T> {
        if (bodyOrSignal instanceof AbortSignal) {
            signal = bodyOrSignal;
            bodyOrSignal = null;
        }

        let trigger: (highPriority?: boolean) => void;
        const promise = this.reqPromise<T>(
            v => (trigger = v),
            endpointName,
            query,
            bodyOrSignal as BodyType,
            signal
        );
        return susFromPromise(promise, trigger);
    }
}
