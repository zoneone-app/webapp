import {addMinutes, startOfDay} from "date-fns";
import {Api, InitTransparent} from "./Api";
import {SusRes, Triggerable} from "./types";
import {getReqValRes} from "./utils";
import Route from "./Route";
import Stop from "./Stop";
import {Root} from "./Root";

interface TripStopInfo {
    stopId: string;
    arrivalOffset: number;
    isTimingPoint: boolean;
    dwell: number;
}

export interface TripInfo {
    id: string;
    routeId: string;
    stoppingPatternId: string;
    startTime: number;
    stops: TripStopInfo[];
}

export class TripStop extends Api<Root> implements Triggerable {
    public readonly stop: Stop;
    private readonly tripStopInfo: TripStopInfo;
    private readonly tripStartTime: SusRes<Date>;

    constructor(
        root: Root,
        tripStopInfo: TripStopInfo,
        tripStartTime: SusRes<Date>
    ) {
        // never need to do any api calls directly
        super(null, root);

        this.tripStopInfo = tripStopInfo;
        this.tripStartTime = tripStartTime;
        this.stop = root
            .stops()
            .getById(this.tripStopInfo.stopId, null, {trips: false});
    }

    get stopId() {
        return this.tripStopInfo.stopId;
    }

    get arrivalOffset() {
        return this.tripStopInfo.arrivalOffset;
    }

    get isTimingPoint() {
        return this.tripStopInfo.isTimingPoint;
    }

    get dwell() {
        return this.tripStopInfo.dwell;
    }

    get arrivalTime() {
        return this.extendSus("arrivalTime", this.tripStartTime, v =>
            addMinutes(v, this.arrivalOffset)
        );
    }

    trigger(highPriority?: boolean) {
        this.stop.trigger(highPriority);
    }
}

interface EagerRequests {
    route?: boolean;
    stops?: boolean;
    lastStop?: boolean;
    stoppingPattern?: boolean;
}

export default class Trip extends Api<Root> implements Triggerable {
    public readonly route: SusRes<Route>;
    private readonly tripInfo: SusRes<TripInfo>;
    private readonly startOfDay = startOfDay(new Date());
    private readonly eagerRequests: EagerRequests;

    constructor(
        init: InitTransparent,
        root: Root,
        id: string,
        tripInfo: SusRes<TripInfo>,
        eagerRequests?: EagerRequests
    ) {
        super(init, root, {id});

        this.tripInfo = tripInfo;
        this.eagerRequests = eagerRequests;
        this.route = this.extendSus("route", this.tripInfo, v =>
            root.routes().getById(v.routeId)
        );
    }

    get id() {
        return this.autoSusEx("id", this.tripInfo);
    }

    get startTime() {
        return this.extendSus("startTime", this.tripInfo, v =>
            addMinutes(this.startOfDay, v.startTime)
        );
    }

    get stops() {
        return this.extendSus(
            "stops",
            this.tripInfo,
            v => v.stops.map(s => new TripStop(this.root, s, this.startTime)),
            v => v.forEach(ts => ts.trigger(true))
        );
    }

    get stoppingPattern() {
        return this.extendSus("stoppingPattern", this.tripInfo, v =>
            this.root.stoppingPatterns().getById(v.stoppingPatternId)
        );
    }

    get lastStop() {
        return this.getStopByIdx(-1);
    }

    trigger(highPriority?: boolean) {
        this.logTrigger();
        this.tripInfo.trigger(highPriority);
        if (this.eagerRequests?.route !== false) this.route.trigger(true);
        if (this.eagerRequests?.stops !== false) this.stops.trigger(true);
        if (this.eagerRequests?.lastStop !== false) this.lastStop.trigger(true);
        if (this.eagerRequests?.stoppingPattern !== false)
            this.stoppingPattern.trigger(highPriority);
    }

    isLoading() {
        return (
            this.tripInfo.loading ||
            (this.eagerRequests?.route !== false && this.route.loading) ||
            (this.eagerRequests?.stops !== false && this.stops.loading) ||
            (this.eagerRequests?.lastStop !== false && this.lastStop.loading) ||
            (this.eagerRequests?.stoppingPattern !== false &&
                this.stoppingPattern.loading)
        );
    }

    /**
     * Gets a stop by the specified index. Negative index counts from end (-1=last)
     */
    getStopByIdx(idx: number) {
        return this.extendSus("stop:" + idx, this.tripInfo, v => {
            const index = idx < 0 ? v.stops.length + idx : idx;
            if (index > v.stops.length)
                throw new RangeError(
                    `Index ${idx} (=${index}) is out of range`
                );
            return new TripStop(this.root, v.stops[index], this.startTime);
        });
    }

    getStopById(id: string) {
        return this.extendSus("stop:" + id, this.tripInfo, v => {
            const el = v.stops.find(v => v.stopId === id);
            if (typeof el === "undefined")
                throw new RangeError(`Stop ${id} does not exist for this trip`);
            return new TripStop(this.root, el, this.startTime);
        });
    }
}

export class Trips extends Api<Root> {
    private tripCache: Map<string, Trip> = new Map();

    constructor(init: InitTransparent, root: Root) {
        super(init, root);
    }

    getById(id: string, signal?: AbortSignal, eagerRequests?: EagerRequests) {
        const actualId = getReqValRes(id);
        if (typeof actualId === "undefined")
            throw new Error("ID is not defined");

        if (this.tripCache.has(actualId)) return this.tripCache.get(actualId);

        const trip = new Trip(
            this.init,
            this.root,
            id,
            this.req<TripInfo>("Trips:GetTrip", {id}, signal),
            eagerRequests
        );

        this.tripCache.set(actualId, trip);
        return trip;
    }
}
