import {startOfDay, addMinutes} from "date-fns";
import {Triggerable} from "./types";
import Route from "./Route";
import {Api, InitTransparent} from "./Api";
import StoppingPattern from "./StoppingPattern";
import Trip from "./Trip";
import {Root} from "./Root";

export interface StopTripInfo {
    tripId: string;
    routeId: string;
    stoppingPatternId: string;
    arrivalTime: number;
    dwellDuration: number;
    days: number;
}

export default class StopTrip extends Api<Root> implements Triggerable {
    private readonly stopTripInfo: StopTripInfo;
    private readonly startOfDay = startOfDay(new Date());

    public readonly route: Route;
    public readonly stoppingPattern: StoppingPattern;
    public readonly trip: Trip;

    constructor(init: InitTransparent, root: Root, stopTripInfo: StopTripInfo) {
        super(init, root);
        this.stopTripInfo = stopTripInfo;
        this.route = root.routes().getById(stopTripInfo.routeId);
        this.stoppingPattern = root
            .stoppingPatterns()
            .getById(stopTripInfo.stoppingPatternId);
        this.trip = root
            .trips()
            .getById(stopTripInfo.tripId, null, {route: false, stops: false});
    }

    trigger() {
        this.logTrigger();
        this.route.trigger(true);
        this.stoppingPattern.trigger(true);
        this.trip.trigger(false);
    }

    isLoading() {
        return (
            this.route.isLoading() ||
            this.stoppingPattern.isLoading() ||
            this.trip.isLoading()
        );
    }

    get tripId() {
        return this.stopTripInfo.tripId;
    }

    get routeId() {
        return this.stopTripInfo.routeId;
    }

    get arrivalTime() {
        return addMinutes(this.startOfDay, this.stopTripInfo.arrivalTime);
    }

    get dwellDuration() {
        return this.stopTripInfo.dwellDuration;
    }

    get days() {
        return this.stopTripInfo.days;
    }
}
