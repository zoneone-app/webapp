import noop from "../noop";
import {Path, PathValue} from "../Path";
import {ReqVal, Sus, SusRes, Triggerable} from "./types";

export function getReqValRes<T>(val: ReqVal<T>): T {
    if (typeof val === "function") return val();
    return val as T;
}

export function isTriggerable(val: unknown): val is Triggerable {
    if (val == null) return false;
    if (typeof val !== "object") return false;
    if (!("trigger" in val)) return false;
    return typeof (val as {trigger: unknown}).trigger === "function";
}

/**
 * Runs the callback on the result of the Sus. Kind of like .then for promises, but with some extra
 * functionality.
 * @param src - Source SusRes to wait on
 * @param map - Function to get the result value
 * @param triggerFn - Function to trigger the map'd result
 * @return SusRes that will complete with the map'd result after the src has completed
 */
export function extendSus<TV, TR>(
    src: SusRes<TV>,
    map: (val: TV) => TR,
    triggerFn?: (val: TR) => void
) {
    const {trigger, promise: basePromise} = src;

    // if the map function returns a Triggerable, trigger it when trigger is called
    let triggered = false,
        isHighPriority = false,
        secondTrigger: (highPriority?: boolean) => void = noop;

    const promise = basePromise.then(map).then(v => {
        if (typeof triggerFn === "function") triggerFn(v);
        else if (isTriggerable(v)) {
            if (triggered) v.trigger(isHighPriority);
            else secondTrigger = v.trigger.bind(v);
        }

        return v;
    });

    const triggerBoth = (highPriority?: boolean) => {
        isHighPriority = highPriority;
        trigger(highPriority);
        secondTrigger(highPriority);
        triggered = true;
    };

    return susFromPromise(promise, triggerBoth);
}

/**
 * Calls {@see extendSus} with an automatic `map` function, generated from the supplied path
 * @param path - Path in dot notation
 * @param src - Source SusRes to wait on
 * @param triggerFn - Function to trigger the map'd result
 * @return SusRes that will complete with the map'd result after the src has completed
 */
export function autoSusEx<TV, TP extends Path<TV>>(
    path: TP,
    src: SusRes<TV>,
    triggerFn?: (val: PathValue<TV, TP>) => void
): SusRes<PathValue<TV, TP>> {
    type Value = PathValue<TV, TP>;

    const map = (res: TV): Value => {
        let obj = res as Record<string | number, unknown>;

        for (const pathPart of (path as string).split(".")) {
            const key =
                Array.isArray(obj) && !Number.isNaN(parseInt(pathPart))
                    ? parseInt(pathPart)
                    : pathPart;
            obj = obj[key] as Record<string | number, unknown>;
        }

        return obj as Value;
    };

    return extendSus(src, map, triggerFn);
}

/**
 * Runs the callback on the result of the Sus. Kind of like .then for promises, but with some extra
 * functionality.
 * @param src - Source SusRes to wait on
 * @param map - Function to get the result value
 * @param triggerFn - Function to trigger the map'd result
 * @return SusRes that will complete with the map'd result after both the src and map result have completed
 */
export function extendSusRes<TV, TR>(
    src: SusRes<TV>,
    map: (val: TV) => SusRes<TR>,
    triggerFn?: (val: TR) => void
) {
    const {trigger, promise: basePromise} = src;

    // if the map function returns a Triggerable, trigger it when trigger is called
    let triggered = false,
        isHighPriority = false,
        secondTrigger: (highPriority?: boolean) => void = noop;

    // the only difference with extendSus is that we do `.then(v => v.promise)` here.
    // unfortunately that is something we have to completely rewrite the function for
    const promise = basePromise
        .then(map)
        .then(v => v.promise)
        .then(v => {
            if (typeof triggerFn === "function") triggerFn(v);
            else if (isTriggerable(v)) {
                if (triggered) v.trigger(isHighPriority);
                else secondTrigger = v.trigger.bind(v);
            }

            return v;
        });

    const triggerBoth = (highPriority?: boolean) => {
        isHighPriority = highPriority;
        trigger(highPriority);
        secondTrigger(highPriority);
        triggered = true;
    };

    return susFromPromise(promise, triggerBoth);
}

class ExtensibleFunction extends Function {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/ban-types
    constructor(f: Function) {
        return Object.setPrototypeOf(f, new.target.prototype);
    }
}

interface CallableSusRes<T> {
    (): T;
}

class CallableSusRes<T> extends ExtensibleFunction implements SusRes<T> {
    readonly trigger: () => void;
    readonly suspense: Sus<T>;
    readonly promise: Promise<T>;
    private readonly getState: () => ResourceState;
    private readonly getValue: () => T;

    constructor(
        trigger: () => void,
        suspense: Sus<T>,
        promise: Promise<T>,
        getState: () => ResourceState,
        getValue: () => T
    ) {
        super(function callableSusRes() {
            return suspense();
        });
        this.trigger = trigger;
        this.suspense = suspense;
        this.promise = promise;
        this.getState = getState;
        this.getValue = getValue;
    }

    get done() {
        return this.getState() === ResourceState.resolved;
    }

    get loading() {
        return this.getState() === ResourceState.pending;
    }

    get errored() {
        return this.getState() === ResourceState.rejected;
    }

    get value() {
        return this.getValue();
    }
}

enum ResourceState {
    pending,
    resolved,
    rejected
}

export function susFromPromise<T>(
    promise: Promise<T>,
    trigger = noop
): SusRes<T> {
    let state: ResourceState = ResourceState.pending;
    let result: T, error: unknown;

    promise
        .then(v => {
            result = v;
            state = ResourceState.resolved;
        })
        .catch(err => {
            error = err;
            state = ResourceState.rejected;
        });

    const suspense = () => {
        switch (state) {
            case ResourceState.pending:
                throw promise;
            case ResourceState.resolved:
                return result;
            case ResourceState.rejected:
                throw error;
        }
    };

    return new CallableSusRes(
        trigger,
        suspense,
        promise,
        () => state,
        () => result
    );
}

export function instantSus<T>(value: T, trigger = noop): SusRes<T> {
    return new CallableSusRes(
        trigger,
        () => value,
        null,
        () => ResourceState.resolved,
        () => value
    );
}
