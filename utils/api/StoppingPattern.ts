import {Api, InitTransparent} from "./Api";
import {SusRes, Triggerable} from "./types";
import {getReqValRes} from "./utils";
import {Root} from "./Root";

export enum ExtendedVariation {
    Normal,
    SpeedyCat,
    SkipsStops,
    ExtraStops,
    Alternate
}

export enum Variation {
    Normal,
    Short,
    Extra,
    Alternate
}

export enum Dir {
    A,
    B
}

export interface StoppingPatternInfo {
    id: string;
    routeId: string;
    direction: Dir;
    destination: string;
    startVariation: Variation;
    middleVariation: ExtendedVariation;
    endVariation: Variation;
}

interface EagerRequests {
    path?: boolean;
}

type PathSchema = GeoJSON.Feature<GeoJSON.LineString>;

export default class StoppingPattern extends Api<Root> implements Triggerable {
    private readonly stoppingPatternInfo: SusRes<StoppingPatternInfo>;
    private pathCache: SusRes<PathSchema>;

    constructor(
        init: InitTransparent,
        root: Root,
        id: string,
        stoppingPatternInfo: SusRes<StoppingPatternInfo>,
        signal?: AbortSignal,
        eagerRequests?: EagerRequests
    ) {
        super(init, root, {id});
        this.stoppingPatternInfo = stoppingPatternInfo;

        // eagerly load subvalues
        if (eagerRequests?.path != false) this.path(signal);
    }

    trigger(highPriority?: boolean) {
        this.logTrigger();
        this.stoppingPatternInfo.trigger(highPriority);
        this.pathCache?.trigger(highPriority);
    }

    isLoading() {
        return this.stoppingPatternInfo.loading;
    }

    path(signal?: AbortSignal) {
        if (this.pathCache) return this.pathCache;

        const path = this.req<PathSchema>(
            "StoppingPatterns:GetPath",
            Api.defaultQuery,
            signal
        );
        this.pathCache = path;

        return path;
    }

    get id() {
        return this.autoSusEx("id", this.stoppingPatternInfo);
    }
    get routeId() {
        return this.autoSusEx("routeId", this.stoppingPatternInfo);
    }
    get direction() {
        return this.autoSusEx("direction", this.stoppingPatternInfo);
    }
    get destination() {
        return this.autoSusEx("destination", this.stoppingPatternInfo);
    }
    get startVariation() {
        return this.autoSusEx("startVariation", this.stoppingPatternInfo);
    }
    get middleVariation() {
        return this.autoSusEx("middleVariation", this.stoppingPatternInfo);
    }
    get endVariation() {
        return this.autoSusEx("endVariation", this.stoppingPatternInfo);
    }
}

export class StoppingPatterns extends Api<Root> {
    private stoppingPatternCache: Map<string, StoppingPattern> = new Map();

    constructor(init: InitTransparent, root: Root) {
        super(init, root);
    }

    getById(id: string, signal?: AbortSignal) {
        const actualId = getReqValRes(id);

        if (this.stoppingPatternCache.has(actualId))
            return this.stoppingPatternCache.get(actualId);

        const stoppingPattern = new StoppingPattern(
            this.init,
            this.root,
            id,
            this.req<StoppingPatternInfo>(
                "StoppingPatterns:GetStoppingPattern",
                {id},
                signal
            )
        );

        this.stoppingPatternCache.set(actualId, stoppingPattern);
        return stoppingPattern;
    }
}
