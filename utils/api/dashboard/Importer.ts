import {Api, InitTransparent} from "../Api";
import {SusRes, Triggerable} from "../types";
import {Root} from "../Root";

interface BaseImportingInfo {}

interface NotImportingInfo extends BaseImportingInfo {
    isImporting: false;
}

interface ImportingInfo {
    isImporting: true;
    importData: {
        startTime: number;
        sourceHash: string;
    };
}

export type ImporterInfo = NotImportingInfo | ImportingInfo;

export default class Importer extends Api<Root> implements Triggerable {
    private readonly importerInfo: SusRes<ImporterInfo>;

    constructor(
        init: InitTransparent,
        root: Root,
        importerInfo: SusRes<ImporterInfo>
    ) {
        super(init, root);
        this.importerInfo = importerInfo;
    }

    trigger(highPriority?: boolean): void {
        this.logTrigger();
        this.importerInfo.trigger(highPriority);
    }

    isLoading() {
        return this.importerInfo.loading;
    }

    get isImporting() {
        return this.autoSusEx("isImporting", this.importerInfo);
    }

    setImporting(state: boolean, signal?: AbortSignal) {
        return this.req<void>(
            "Dashboard:SetImporterState",
            Api.defaultQuery,
            {isImporting: state.toString()},
            signal
        );
    }
}
