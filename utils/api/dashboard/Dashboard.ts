import {Api, InitTransparent} from "../Api";
import {Root} from "../Root";
import Importer, {ImporterInfo} from "./Importer";

export default class Dashboard extends Api<Root> {
    private _importer: Importer;

    constructor(init: InitTransparent, root: Root) {
        super(init, root);
    }

    importer(signal?: AbortSignal) {
        if (this._importer) return this._importer;
        this._importer = new Importer(
            this.init,
            this.root,
            this.req<ImporterInfo>(
                "Dashboard:GetImporterState",
                Api.defaultQuery,
                signal
            )
        );
        return this._importer;
    }
}
