import {differenceInCalendarDays} from "date-fns";

const epoch = Date.UTC(2021, 0, 1);

export function getEpochFromDate(date: Date) {
    return differenceInCalendarDays(date, epoch);
}
