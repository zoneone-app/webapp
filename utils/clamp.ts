export default function clamp(a: number, b: number, v: number) {
    const min = a < b ? a : b;
    const max = a > b ? a : b;

    if (v < min) return min;
    if (v > max) return max;
    return v;
}
