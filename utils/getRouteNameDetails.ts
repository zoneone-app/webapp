import {SidebarLabelLines} from "../components/sidebar/heading/SidebarLabel";

export function getRouteNameDetails(
    lastStopStationName: string,
    lastStopSuburb: string
): SidebarLabelLines {
    const topLine = lastStopStationName || lastStopSuburb;

    return {lineOne: topLine};
}
