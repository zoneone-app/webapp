import {Heading, Flex, Text, Box, Center} from "@chakra-ui/react";
import React, {FC, Suspense, useEffect} from "react";
import {withLayout} from "@moxy/next-layout";
import {Layout} from "../../components/Layout";
import {useQuery, WaitForQuery} from "../../components/util/WaitForQuery";
import {
    PageData,
    PageDataConsumer,
    useDefaultPageData,
    usePageData
} from "../../utils/pageData";
import Stop from "../../utils/api/Stop";
import {SusRes} from "../../utils/api/types";
import StopTrip from "../../utils/api/StopTrip";
import {StopTripDetail} from "../../components/StopTripDetail";
import {Link} from "../../components/Link";
import {StopLabel} from "../../components/sidebar/heading/StopLabel";
import {ThreeDotsSpinner} from "../../components/spinners/ThreeDotsSpinner";
import {useApi} from "../../utils/api/useApi";

const StopIdLabel: FC<{id: SusRes<number>}> = props => {
    const href = `https://jp.translink.com.au/plan-your-journey/stops/${props.id()}`;

    return (
        <Link target="_blank" href={href}>
            {props.id().toString().padStart(6, "0")}
        </Link>
    );
};

const StopId: FC<{id: SusRes<number>}> = props => (
    <Text textStyle="footnote" align="right" mt={1}>
        ID:
        <Suspense fallback={<span>...</span>}>
            <StopIdLabel id={props.id} />
        </Suspense>
    </Text>
);

const StopHeading: FC<{stop: Stop}> = props => {
    return (
        <Box padding={4} paddingBottom={0}>
            <StopLabel
                size="lg"
                name={props.stop.name}
                suburb={props.stop.suburb}
            />
            <StopId id={props.stop.gtfsId} />
        </Box>
    );
};

const TripList: FC<{trips: SusRes<StopTrip[]>; stop: Stop}> = props => {
    const trips = props.trips();

    // trigger all the trips
    useEffect(() => trips.forEach(v => v.trigger()), [trips]);

    return (
        <Box overflow="auto" flexBasis="fill">
            {trips.length ? (
                trips.map((trip, i) => (
                    <StopTripDetail
                        key={trip.tripId}
                        trip={trip}
                        stop={props.stop}
                        isPreviousLoading={trips[i - 1]?.isLoading()}
                    />
                ))
            ) : (
                <Heading
                    textStyle="footnote"
                    paddingTop={4}
                    paddingLeft={4}
                    align="center"
                >
                    No trips stop here today
                </Heading>
            )}
        </Box>
    );
};

const Loader: FC = () => (
    <Center mt={6}>
        <ThreeDotsSpinner />
    </Center>
);

const Sidebar: FC<{stop: Stop}> = ({stop}) => {
    return (
        <Flex direction="column" height={["auto", "calc(100vh - 3.25rem)"]}>
            <Suspense fallback={<Loader />}>
                <StopHeading stop={stop} />
                <Heading
                    textStyle="footnote"
                    color="gray.400"
                    paddingBottom={2}
                    paddingLeft={4}
                    borderBottomWidth="1px"
                    borderBottomColor="gray.200"
                >
                    Departures
                </Heading>
                <TripList trips={stop.trips(new Date())} stop={stop} />
            </Suspense>
        </Flex>
    );
};

const StopPage: FC = () => {
    const api = useApi();

    const loadStop = (id: string) => {
        const stop = api.stops().getById(id);

        return {
            stop: {id, value: stop}
        };
    };

    return (
        <WaitForQuery fallback={<Loader />}>
            {query => (
                <PageDataConsumer
                    map={v => v.stop}
                    load={loadStop}
                    loadParams={[query.id as string]}
                    fallback={<Loader />}
                >
                    {data => <Sidebar stop={data.stop.value} />}
                </PageDataConsumer>
            )}
        </WaitForQuery>
    );
};

export default withLayout(<Layout />)(StopPage);
