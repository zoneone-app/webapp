import React, {FC} from "react";
import {withLayout} from "@moxy/next-layout";
import {Layout} from "../components/Layout";

const Index: FC = () =>
    // ignore page data changes, we don't need it
    null;

export default withLayout(<Layout />)(Index);
