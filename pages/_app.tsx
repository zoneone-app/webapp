import React, {FC} from "react";
import {ChakraProvider} from "@chakra-ui/react";
import {AppProps} from "next/app";
import Head from "next/head";
import {AppState, Auth0Provider} from "@auth0/auth0-react";
import router from "next/router";
import {LayoutTree} from "@moxy/next-layout";
import {theme} from "../style/theme";
import ErrorBoundary from "../components/ErrorBoundary";
import {PageDataProvider} from "../utils/pageData";
import {ApiProvider} from "../utils/api/useApi";

const onRedirectCallback = (appState: AppState) =>
    router.replace(appState?.returnTo || "/");

const App: FC<AppProps> = ({Component, pageProps}) => (
    <>
        <Head>
            <title>ZoneONE</title>
        </Head>
        <ErrorBoundary>
            <ChakraProvider theme={theme}>
                <Auth0Provider
                    domain={process.env.NEXT_PUBLIC_AUTH0_DOMAIN}
                    clientId={process.env.NEXT_PUBLIC_AUTH0_CLIENT_ID}
                    redirectUri={
                        typeof window !== "undefined" && window.location.origin
                    }
                    onRedirectCallback={onRedirectCallback}
                >
                    <ApiProvider
                        baseUrl={process.env.NEXT_PUBLIC_API_ENDPOINT}
                        audience={process.env.NEXT_PUBLIC_API_AUDIENCE}
                    >
                        <PageDataProvider>
                            <LayoutTree
                                Component={Component}
                                pageProps={pageProps}
                            />
                        </PageDataProvider>
                    </ApiProvider>
                </Auth0Provider>
            </ChakraProvider>
        </ErrorBoundary>
    </>
);

export default App;
