import {UrlObject} from "url";
import React, {FC, Suspense} from "react";
import {Box, Flex, Heading, Text, chakra} from "@chakra-ui/react";
import {addMinutes} from "date-fns";
import {withLayout} from "@moxy/next-layout";
import {PageDataConsumer} from "../../utils/pageData";
import {Layout} from "../../components/Layout";
import {WaitForQuery} from "../../components/util/WaitForQuery";
import Trip, {TripStop} from "../../utils/api/Trip";
import {SusRes} from "../../utils/api/types";
import {RouteLabelSus} from "../../components/sidebar/heading/RouteLabel";
import {ListingLink} from "../../components/ListingLink";
import {TripDetailContainer} from "../../components/TripDetail";
import {StopLabel} from "../../components/sidebar/heading/StopLabel";
import {TimeDisplay} from "../../components/TimeDisplay";
import {extendSusRes} from "../../utils/api/utils";
import {useApi} from "../../utils/api/useApi";

const Loader: FC = () => (
    <Flex align="center" justify="center" height="100%">
        <Text>Loading</Text>
    </Flex>
);

const TripHeading: FC<{trip: Trip}> = props => {
    const {code, styling} = props.trip.route();
    const lastStop = props.trip.getStopByIdx(-1);

    return (
        <Box padding={4}>
            <RouteLabelSus
                lastStop={lastStop}
                code={code}
                styling={styling}
                size="lg"
            />
        </Box>
    );
};

const SideLineLine = chakra("div", {
    baseStyle: {
        background: "blue.700",
        width: ".4rem",

        position: "absolute",
        top: 0,
        bottom: 0,
        left: "50%",
        transform: "translateX(-50%)"
    }
});

enum SideLineVariation {
    line,
    none
}

const SideLine: FC<{variation: SideLineVariation}> = props => {
    if (props.variation === SideLineVariation.none) return <></>;

    return <SideLineLine zIndex={1} height="calc(100% + 1rem)" top="50%" />;
};

const TripSide: FC<{variation: SideLineVariation}> = props => (
    <Box position="relative" minWidth={5}>
        <SideLine variation={props.variation} />
        <Flex justify="center" position="relative" zIndex={2} height="100%">
            {props.children}
        </Flex>
    </Box>
);

const StopDetail: FC<{stop: TripStop; line: SideLineVariation}> = ({
    stop,
    line
}) => {
    const linkHref: UrlObject = {
        pathname: "/stops/[id]",
        query: {
            id: stop.stopId
        }
    };

    const approx = !(stop.isTimingPoint || stop.dwell > 0);

    return (
        <ListingLink href={linkHref}>
            <TripDetailContainer>
                <StopLabel
                    name={stop.stop.name}
                    size="md"
                    details={
                        <TimeDisplay
                            time={stop.arrivalTime()}
                            endTime={
                                stop.dwell &&
                                addMinutes(stop.arrivalTime(), stop.dwell)
                            }
                            approximate={approx}
                            preText="Sch "
                        />
                    }
                    sideContainer={child => (
                        <TripSide variation={line}>{child}</TripSide>
                    )}
                />
            </TripDetailContainer>
        </ListingLink>
    );
};

const StopList: FC<{stops: SusRes<TripStop[]>}> = props => {
    const stops = props.stops();

    return (
        <Box overflow="auto">
            {stops.map((stop, i) => {
                const line =
                    i === stops.length - 1
                        ? SideLineVariation.none
                        : SideLineVariation.line;

                return (
                    <StopDetail
                        stop={stop}
                        line={line}
                        key={`${stop.arrivalOffset}/${stop.stopId}`}
                    />
                );
            })}
        </Box>
    );
};

const Sidebar: FC<{trip: Trip}> = ({trip}) => {
    return (
        <Flex direction="column" height={["auto", "calc(100vh - 3.25rem)"]}>
            <Suspense fallback={<Loader />}>
                <TripHeading trip={trip} />
            </Suspense>
            <Suspense fallback={<Loader />}>
                <Heading
                    textStyle="footnote"
                    color="gray.400"
                    paddingBottom={2}
                    paddingLeft={4}
                    borderBottomWidth="1px"
                    borderBottomColor="gray.200"
                >
                    Stops
                </Heading>
                <StopList stops={trip.stops} />
            </Suspense>
        </Flex>
    );
};

const TripPage: FC = () => {
    const api = useApi();

    const loadTrip = (id: string) => {
        const trip = api.trips().getById(id);

        return {
            trip: {id, value: trip},
            mapGeoJson: extendSusRes(trip.stoppingPattern, v => v.path())
        };
    };

    return (
        <WaitForQuery fallback={<Loader />}>
            {query => (
                <PageDataConsumer
                    map={v => v.trip}
                    load={loadTrip}
                    loadParams={[query.id as string]}
                    fallback={<Loader />}
                >
                    {data => <Sidebar trip={data.trip.value} />}
                </PageDataConsumer>
            )}
        </WaitForQuery>
    );
};

export default withLayout(<Layout />)(TripPage);
