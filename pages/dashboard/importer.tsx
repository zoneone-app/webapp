import React, {FC, useCallback} from "react";
import {withAuthenticationRequired} from "@auth0/auth0-react";
import {Button, HStack} from "@chakra-ui/react";
import {PageDataConsumer, usePageData} from "../../utils/pageData";
import {useApi} from "../../utils/api/useApi";

const ControlButton: FC = () => {
    const {dashImporter: importer} = usePageData();

    const isImporting = importer.isImporting();

    const handleClick = useCallback(() => {
        if (isImporting) {
            console.warn("Cannot cancel import");
        } else {
            importer.setImporting(true).trigger();
        }
    }, [isImporting]);

    const label = isImporting ? "Cancel" : "Begin import";
    const colourScheme = isImporting ? "red" : "blue";

    return (
        <Button colorScheme={colourScheme} onClick={handleClick}>
            {label}
        </Button>
    );
};

const ImporterPage: FC = () => {
    const api = useApi();

    return (
        <PageDataConsumer
            fallback={<p>...</p>}
            map={v => v.dashImporter}
            load={() => ({dashImporter: api.dashboard().importer()})}
        >
            <HStack>
                <ControlButton />
            </HStack>
        </PageDataConsumer>
    );
};

export default ImporterPage;
